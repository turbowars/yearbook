var image;


$(document).ready(function() {

    //Initiate date picker
    $('#date').datepicker();

    //File upload check
    if (window.File && window.FileList && window.Blob && window.FileReader && window.FormData) {
        //
    } else {
        alert("Your browser does not support file upload!! :(");
    }

    // Get the uploaded image
    $('#image_file').change(function(e) {
        if (e.target.files[0]) {
            image = e.target.files[0];
        }
    });

});

function validateForm() {
    $(".validation-msgs").empty();
    var validCheck = true;

    //Check if title exists
    if ($("#title").val() == "") {
        $(".validation-msgs").append("<li>Kindly enter a title</li>");
        validCheck = false;
    }

    //Check if date exists
    if ($("#date").val() == "") {
        $(".validation-msgs").append("<li>Kindly enter a date</li>");
        validCheck = false;
    }

    //Check if image exists
    if (image == undefined) {
        $(".validation-msgs").append("<li>Kindly upload an image</li>");
        validCheck = false;
    }
    return validCheck;
}



// this is the id of the form
$("#superform").submit(function(e) {

	$('.btn-submit').attr('disabled',true);

    var url = "http://4fb489f6.ngrok.io/post"; // the script where you handle the form input.
    var title = $("#title").val();
    var tags = $("#tags").val();
    var description = $("#description").val();

    var date_raw = $("#date").val();
    var date_format = new Date(date_raw);
    var date_server = date_format.getTime();


    var form = new FormData();
    form.append("title", title);
    form.append("description", description);
    form.append("tags", tags);
    form.append("image", image);
    form.append("image_date", date_server);

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": "POST",
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        "data": form
    }

    if (validateForm()) {
        $(".validation-msgs").empty();
        $.ajax(settings).done(function(response) {
            //console.log(response);
            $("#superform").trigger("reset");
            $('.success-msg').show().delay(5000).fadeOut();
            $('.btn-submit').attr('disabled',false);
        });
    } else {
    	$('.btn-submit').attr('disabled',false);
        console.log('Form not validated');
    }
    // avoid to execute the actual submit of the form.
    e.preventDefault();
});
